# Examples of how to convert LT services into ELG compatible services

- [Example 1: Simple Python Language identification service](example1_simple_python_language_identification_service/README.md)
- [Example 2: NER model from Huggingface](example2_ner_model_from_huggingface/README.md)
- [Example 3: Welsh Speech to Text service using an ELG adapter](example3_welsh_speech_to_text_using_adapter/README.md)