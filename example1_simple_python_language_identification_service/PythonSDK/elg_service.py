from elg import FlaskService
from elg.model import ClassificationResponse

import langdetect

class ELGService(FlaskService):
    
    def process_text(self, content):
        langs = langdetect.detect_langs(content.content)
        ld = []
        for l in langs:
            ld.append({
                "class": l.lang,
                "score": l.prob,
            })
        return ClassificationResponse(classes=ld)

flask_service = ELGService("LangDetection")
app = flask_service.app