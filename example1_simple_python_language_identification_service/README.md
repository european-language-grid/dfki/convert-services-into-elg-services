# Example 1: Simple Python Language identification service

In the example, we will see how to create an ELG compatible service that will identify the language of a text. 
To do the language identification, we will use the [`langdetect` package](https://pypi.org/project/langdetect/).

We will see two ways to create the ELG service. The first one is by using FastAPI and the second one is by using the Python SDK which will simplify the process.

## Using FastAPI

### Step 0: Set up the environment

Before starting, we will set up a new environment. Let's create a new folder:

```bash
git clone https://gitlab.com/european-language-grid/dfki/convert-services-into-elg-services
cd convert-services-into-elg-services/example1_simple_python_language_identification_service/FastAPI
```

And now a new environment:

```bash
conda create -n example1-FastAPI python=3.7
conda activate example1-FastAPI
```

We will also install the packages needed:

```bash
pip install elg langdetect fastapi uvicorn[standard]
```

### Step 1: Use the `langdetect` package

We can firstly test the package to see how does it work:

```python
>>> import langdetect
>>> print(langdetect.detect_langs("This is a text in English."))
[en:0.9999979910220975]
```

### Step 2: Create the FastAPI application

We will create the file `app/main.py` that contains the FastAPI application:

```python
from fastapi import FastAPI
from elg.model import TextRequest, ClassificationResponse

import langdetect

app = FastAPI()

@app.post("/process/")
async def process(request: TextRequest):
    langs = langdetect.detect_langs(request.content)
    ld = []
    for l in langs:
        ld.append({
            "class": l.lang,
            "score": l.prob,
        })
    return {"response": ClassificationResponse(classes=ld)}
```

We use the `elg.model` `Request` and `Response` classes to be sure to be ELG compatible.

### Step 3: Create the Docker image

Now that our service is exposing a http endpoint that is compatible with the ELG LT internal API, we have to dockerize the service.
To do so, let's create a Dockerfile:

```
FROM tiangolo/uvicorn-gunicorn-fastapi:python3.7

RUN pip install elg langdetect

COPY ./app /app
```

Here we have to specify the Pipy package we have to install.

After that we just have to build the image:

```bash
docker build -t example1_language_identification_fastapi:0.1 .
```

### Step 4: Test the service

To test the service, we will use the Python SDK that will send a `elg.model.TextRequest` to the service.

So we first need to run the Docker image:

```
docker run -p 127.0.0.1:8585:80 example1_language_identification_fastapi:0.1
```

and then to call it using the Python SDK:

```python
>>> from elg import Service
>>> service = Service.from_docker_image("example1_language_identification_fastapi:0.1", "hhpt://localhost:80/process", 8585)
>>> service("English text", sync_mode=True)
ClassificationResponse(type='classification', warnings=None, classes=[ClassesResponse(class_field='en', score=0.9999969043070958)])
```

### Step 5: Upload the Docker image in a Docker Registry

Here you can choose the Docker Registry of your choice.

## Using the Python SDK

### Step 0: Set up the environment

Before starting, we will set up a new environment. Let's create a new folder:

```bash
git clone https://gitlab.com/european-language-grid/dfki/convert-services-into-elg-services
cd convert-services-into-elg-services/example1_simple_python_language_identification_service/PythonSDK
```

And now a new environment:

```bash
conda create -n example1-PythonSDK python=3.7
conda activate example1-PythonSDK
```

We will also install the packages needed:

```bash
pip install elg langdetect
```

### Step 1: Create the Service class

By using the Python SDK, we just have to define a new class that inherites from the `FlaskService` class in the `elg_service.py` file.
As our service is using text as input, we will only define the `process_text` method:

```python
from elg import FlaskService
from elg.model import ClassificationResponse

import langdetect

class ELGService(FlaskService):
    
    def process_text(self, content):
        langs = langdetect.detect_langs(content.content)
        ld = []
        for l in langs:
            ld.append({
                "class": l.lang,
                "score": l.prob,
            })
        return ClassificationResponse(classes=ld)

flask_service = ELGService("LangDetection")
app = flask_service.app
```

### Step 2: Create the Dockerfile and the requirements using the `elg` CLI

The Python SDK comes with a CLI that we can use here to automaticly generate the Dockerfile and the requirements:

```bash
elg docker create --path ./elg_service.py --classname ELGService --requirements langdetect
```

After that we just have to build the image:

```bash
docker build -t example1_language_identification_pythonsdk:0.1 .
```

### Step 4: Test the service

To test the service, we will use the Python SDK that will send a `elg.model.TextRequest` to the service.

So we first need to run the Docker image:

```
docker run -p 127.0.0.1:8585:8000 example1_language_identification_pythonsdk:0.1
```

and then to call it using the Python SDK:

```python
>>> from elg import Service
>>> service = Service.from_docker_image("example1_language_identification_pythonsdk:0.1", "hhpt://localhost:8000/process", 8585)
>>> service("This is a text in English.", sync_mode=True)
ClassificationResponse(type='classification', warnings=None, classes=[ClassesResponse(class_field='en', score=0.9999969043070958)])
```

### Step 5: Upload the Docker image in a Docker Registry

Here you can choose the Docker Registry of your choice.
