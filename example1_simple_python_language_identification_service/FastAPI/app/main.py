from fastapi import FastAPI
from elg.model import TextRequest, ClassificationResponse

import langdetect

app = FastAPI()

@app.post("/process/")
async def process(request: TextRequest):
    langs = langdetect.detect_langs(request.content)
    ld = []
    for l in langs:
        ld.append({
            "class": l.lang,
            "score": l.prob,
        })
    return {"response": ClassificationResponse(classes=ld)}