# Docker deepspeech CY server with ELG adapter

## Components

The deepspeech CY service is deployed into ELG using the [adapter intergration options](https://european-language-grid.readthedocs.io/en/stable/all/3_Contributing/Service.html#technical-requirements-and-integration-options).

The resulting pod deployed in the ELG cluster will contain 2 docker containers: the non-ELG-compatible service (deepspeech CY server) and the ELG-compatible adatper.

### Deepspeech CY server

#### Overview

All the code of the original deepspeech CY server is inside the `LT_tool` folder. You can find more info in the [README.md](LT_tool/README.md).

> The commands to build and run the service from [README.md](LT_tool/README.md) are not working anymore. Instead please, use the commands from the following section.

#### Build the docker image

```bash
cd LT_tool
make prepare-transcribe # -> download the models
make build-transcribe # -> build the docker image. Equivalent to the following `docker build` command:
# docker build \
#     --rm \
#     -t techiaith/deepspeech-0.9.3-server:transcribe \
#     --build-arg DEEPSPEECH_VERSION=0.9.3 \
#     --build-arg MODEL_NAME=transcribe \
#     --build-arg MODEL_VERSION=21.03 \
#     .
```

### Adapter

#### Overview

The adapter is build using the [ELG python SDK](https://european-language-grid.readthedocs.io/en/stable/all/A1_PythonSDK/PythonSDK.html) [`FlaskService` class](https://european-language-grid.readthedocs.io/en/stable/all/A1_PythonSDK/FlaskService.html).

> A tutorial on how to use the `FlaskService` class is available [here](https://european-language-grid.readthedocs.io/en/stable/all/A1_PythonSDK/TutoServiceIntegration.html).

The function of the adapter is to expose an ELG-compatible endpoint and act as proxy to the container that hosts the actual LT tool.

#### Build the docker image

As the adapter is using the ELG python SDK, you need to have it installed. 
If it is not the case, run: `pip install elg==0.4.5`.

Step 0: Go to the correct folder

```bash
cd adapter
```

Step 1: Create the Dockerfile and the requirements using the `elg` cli

```bash
elg docker create --path ./adapter.py --classname Adapter
```

Step 2: Build the docker image

```bash
docker build -t techiaith/deepspeech-0.9.3-adapter:transcribe .
```

## Test everything

### Run the docker images

Here we will run the 2 docker images on the `host` network which is not ideal but ok for test purposes. We do that to simulate the fact that the 2 containers will run in the same network inside a pod in the ELG kubernetes cluster.

#### Deepspeech CY server

```bash
docker run --name deepspeech-server-transcribe \
    --network host \
    -d -p 8008:8008  \
    techiaith/deepspeech-0.9.3-server:transcribe
```

#### Adapter

```bash
docker run --name deepspeech-adapter-transcribe \
    --network host \
    -p 8000:8000  \
    techiaith/deepspeech-0.9.3-adapter:transcribe
```

### Call the service using the ELG Python SDK

```python
from elg import Service

service = Service.from_docker_image("techiaith/deepspeech-0.9.3-adapter:transcribe", "http://localhost:8000/process", 8000)
result = service("speech.wav", "audio", sync_mode=True) # be sure to have the speech.wav file where you are running python
print(result)
```
