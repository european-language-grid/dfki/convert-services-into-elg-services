from transformers import pipeline

class DistilbertNEREn:
    
    nlp = pipeline("ner", "distilbert-base-cased-finetuned-conll03-english")
    
    def run(self, input_str):
        return self.nlp(input_str)